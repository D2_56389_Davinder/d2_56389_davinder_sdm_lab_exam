create table user (uid int primary key auto_increment,
                   name varchar(100),
                   age int ,
                   email varchar(100));

create table product (pid int primary key auto_increment,
                      pname varchar(100),
                      category varchar(100),
                      uid int,
                      foreign key (uid) references user(uid));                   