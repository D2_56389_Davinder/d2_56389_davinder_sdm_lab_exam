
const express = require('express');
const movieRouter = require('./route/movie');
const cors = require('cors');

const app=express()
app.use(express.json())
app.use(cors ("*"))
app.use("/user1",movieRouter)
app.use("/",(request,response)=>
{
    response.send(`hello everyone`)
})

app.listen(4000,"0.0.0.0",()=>
{
    console.log(`server started on port 4000`)
})