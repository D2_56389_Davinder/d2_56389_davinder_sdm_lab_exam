const express = require('express');
const utils = require('../utils');
const db = require('../db');

const router=express.Router()

router.get("/",(request,response)=>
{
    const {director_name}=request.body
    const connection = db.openconnection()
    const statement = `select movie_id, movie_title, movie_release_date,movie_time from Movie where director_name='${director_name}' `

    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})

router.post("/",(request,response)=>
{
    const {movie_id, movie_title, movie_release_date,movie_time,director_name}=request.body
    const connection = db.openconnection()
    const statement = `insert into Movie (movie_id, movie_title, movie_release_date,movie_time,director_name) values
                       ('${movie_id}','${movie_title}','${movie_release_date}','${movie_time}','${director_name}') `
    console.log(connection)
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
        console.log(response)
    })
})

router.put("/",(request,response)=>
{
    const {movie_id,movie_release_date,movie_time}=request.body
    const connection = db.openconnection()
    const statement = `update Movie set movie_release_date='${movie_release_date}',movie_time ='${movie_time}' where movie_id='${movie_id}'`

    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})

router.delete("/",(request,response)=>
{
    const {movie_id}=request.body
    const connection = db.openconnection()
    const statement = `delete from Movie where movie_id='${movie_id}' `

    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})

module.exports=router