create table Movie (movie_id int primary key,
                    movie_title varchar(100), 
                    movie_release_date Date ,
                    movie_time time,
                    director_name varchar(100));